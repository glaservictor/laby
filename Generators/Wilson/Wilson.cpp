#include <random>
#include <vector>
#include "../../include/LabyGen.h"
#include "../../include/mybitset.h"
#include <stdlib.h>

using namespace std;

int getAccess(int cur, dBit &graphe) {
    int wid = graphe.getDim().first, hei = graphe.getDim().second;
    while (true) {
        int res = rand() % 4;
        if (cur % wid != wid - 1 && res == 0)
            return ++cur;
        if (cur % wid != 0 && res == 1)
            return --cur;
        if (cur + wid < wid * hei && res == 2)
            return cur + wid;
        if (cur - wid > 0 && res == 3)
            return cur - wid;
    }
}

void RandWalk(vector<int> &sommet, dBit &graphe, vector<int> &order, vector<int> &arr, vector<int> &UST) {
    int cur = 42, k = 42, beg;
    for (int p : order)
        if (p != -1) {
            cur = p, k = p, beg = p;
            break;
        }
    while (sommet[cur] != -1) {
        int res = getAccess(cur, graphe);
        arr[cur] = res, cur = res;
        if (sommet[res] == -1)
            break;
    }

    for (long i = 0; i < arr.size(); i++) {
        if (sommet[k] != -1) {
            UST.push_back(k);
            sommet[k] = -1;
            for (int &l : order)
                if (l == k)
                    l = -1;
            k = arr[k];
        } else {
            UST.push_back(k);
            break;
        }
    }
    UST.push_back(-1);
}

vector<int> generation(dBit &graphe) {
    vector<int> UST;
    int wid = graphe.getDim().first, hei = graphe.getDim().second;
    vector<int> arr(hei * wid, -1), order(hei * wid);
    for (int i = 0; i < hei * wid; i++)
        order[i] = i;
    vector<int> sommet(wid * hei, 0);
    shuffle(order.begin(), order.end(), std::mt19937(std::random_device()()));
    sommet[order.back()] = -1;
    order.pop_back();
    for (int k = 0; k < order.size(); k++)
        while (order[k] != -1)
            RandWalk(sommet, graphe, order, arr, UST);
    return UST;
}

void soluceWilson(dBit &graphe, vector<int> res) {
    int k = (graphe.getDim().first - 1) * (graphe.getDim().second);
    for (int i = 0; i < res.size() - 1; i++) {
        int p = res[i] / graphe.getDim().first;
        if ((res[i] == -1 or res[i + 1] == -1))
            continue;
        if (res[i] + 1 == res[i + 1])
            graphe.setAt(res[i] - p, 1);
        if (res[i] - 1 == res[(i + 1)])
            graphe.setAt(res[i] - 1 - p, 1);
        if (res[i] + graphe.getDim().first == res[(i + 1)])
            graphe.setAt(res[i] + k, 1);
        if (res[i] - graphe.getDim().first == res[i + 1])
            graphe.setAt(res[i] + k - graphe.getDim().first, 1);
    }
}
