#include <vector>
#include "../../include/LabyGen.h"
#include "../../include/mybitset.h"

using namespace std;

vector<int> poids;

struct myCmp {
    bool operator()(const int &p1, const int &p2) {
        return poids[p1] >= poids[p2];
    }
};

vector<vector<int>> S;
vector<bool> vu;
priority_queue<int, vector<int>, myCmp> Q;

void addSommet(int p1) {
    vu[p1] = true;
    for (int i : S[p1])
        Q.push(i);
}

void soluce(dBit &graphe, vector<int> &sel) {
    pair<int, int> dim = graphe.getDim();
    int n = dim.first * dim.second;
    int r = graphe.size();
    vu = vector<bool>(r, false);
    S = vector<vector<int>>(n);
    poids = vector<int>(r);
    vector<pair<int, int>> couple(r);
    int k = 0;
    int k2 = 0;
    int count = 1;
    for (int i = 0; i < r; i++) {
        if (k == count * dim.first - 1) {
            k += 1;
            count++;
        }
        if (k >= dim.first * dim.second) {
            couple[i].first = k2;
            couple[i].second = k2 + dim.first;
            k2++;
        } else {
            couple[i].first = k;
            couple[i].second = k + 1;
            k++;
        }
        poids[i] = rand() % 20000;
        S[couple[i].first].push_back(i);
        S[couple[i].second].push_back(i);
    }
    addSommet(0);
    for (int i = 1; i < n; i++) {
        int cur;
        do {
            cur = Q.top();
            Q.pop();
        } while (vu[couple[cur].first] && vu[couple[cur].second]);
        if (vu[couple[cur].first])
            addSommet(couple[cur].second);
        else if (vu[couple[cur].second])
            addSommet(couple[cur].first);
        sel.push_back(cur);
    }
}
