CXX = g++
DEBUG=yes
ifeq ($(DEBUG),yes)
	CXXFLAGS = -Wall -g -std=c++11
else
	CXXFLAGS = -Wall -std=c++11

endif
LDFLAGS = -lsfml-system -lsfml-window -lsfml-graphics
OBJDIR = objet
DIR = ./BitSetLib ./SFML_interface ./Generators/Wilson ./Generators/Prim ./ ./Enum 
SRC = $(foreach dir, $(DIR), $(wildcard $(dir)/*.cpp))
OBJ = $(SRC:%.cpp=%.o)
LIB = $(wildcard include/*.h) 
EXEC = laby


all: $(EXEC) $(OBJDIR)

$(OBJDIR): 
	mkdir $(OBJDIR)

$(OBJDIR)/%o:%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(EXEC): $(OBJ)
	$(CXX) -o $@ $^ $(LDFLAGS) $(CXXFLAGS)


.PHONY: clean mrproper

clean:
	$(foreach dir, $(DIR), rm -rf $(wildcard $(dir)/*.o))

mrproper:
	rm -rf $(EXEC)

