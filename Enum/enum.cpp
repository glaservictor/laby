#include <iostream>
#include <vector>
#include "../include/mybitset.h"
#include "../include/SFML_interface.h"
#include "../include/enum.h"

using namespace std;

void recherche(int, int, dBit&, int&, vector<int>&);
inline int findFirst(int, int, int);
inline int findSecond(int, int, int);
int _find(int, vector<int>&);

int unionFind()
{
	int n, m, count = 0;
	cout << "Rentrez n et m : ";
	cin >> n >> m;
	dBit graph(n, m, false);
	vector<int> ufTab(m*n);
	for(int i = 0; i < (int)ufTab.size(); i++)
		ufTab[i] = i;
	recherche(0, 0, graph, count, ufTab);
	cout << count << '\n';
	manageWin(graph);
	return 0;
}

void recherche(int depth, int nbAr, dBit &graph, int &count, vector<int> &ufTab)
{
	int n = graph.getDim().first, m = graph.getDim().second;
	if(nbAr == n*m-1) { count++; /*manageWin(graph);*/ return; }
	if(depth >= graph.size()) return;
	if(nbAr + graph.size() - depth < n*m-1) return;
	// cas 1 : on met l'arête
	int a = findFirst(n, m, depth); // sommet 1
	int b = findSecond(n, m, depth); // sommet 2
	int ra = _find(a, ufTab); // représentant de a
	int rb = _find(b, ufTab); // représentant de b
	if(ra != rb) // il semblerait que compression de chemin et heuristique du rang soient inutiles. En tout cas de façon systématique. Doit venir qu'il y a plus d'erreurs que de cas vérifiés. cf compression de chemin qui passe de 6 à 33 secondes
	{
		//graph.setAt(depth, true);
		ufTab[ra] = rb;
		recherche(depth+1, nbAr+1, graph, count, ufTab);
		//graph.setAt(depth, false);
		ufTab[ra] = ra; 
	}
	// cas 2 : on la met pas
	recherche(depth+1, nbAr, graph, count, ufTab);
}

inline int findFirst(int n, int m, int id)
{
	if(id < (m-1)*n)
		return id + (id/(m-1));
	return id - (m-1)*n;
}

inline int findSecond(int n, int m, int id)
{
	if(id < (m-1)*n)
		return id + (id/(m-1)) + 1;
	return id - (m-1)*n + m;
}

int _find(int a, vector<int> &ufTab)
{
	if(a != ufTab[a])
		return _find(ufTab[a], ufTab);
	return a;
}
