#include <SFML/Graphics.hpp>
#include "../include/mybitset.h"
#include <SFML/System/Export.hpp>
#include <SFML/System/Time.hpp>
#include <iostream>

using namespace sf;
using namespace std;


void manageWin(dBit &graphe) {
    int a = graphe.getDim().first;
    int b = graphe.getDim().second;
    int Lsize = 900;
    while (Lsize * a / b > 1800)
        Lsize -= 100;
    sf::RenderWindow window(sf::VideoMode(Lsize * a / b, Lsize), "Maze Visualisation Tool");
    int wid = window.getSize().x / a;
    int len = window.getSize().y / b;
    sf::VertexArray line(Lines, 2 * graphe.size() + 1);
    int count = 0;
    int countB = 0;
    int den = 0;
    for (int i = 0; i < graphe.size(); i++) {
        if (i < (a - 1) * b) {
            if (den > a - 2) {
                count++;
                den = 0;
            }
            if (graphe.at(i)) {
                line[2 * i].position = Vector2f(wid * (den + 1), len * (count + 1));
                line[2 * i + 1].position = Vector2f((den + 2) * wid, len * count + len);
            }

        } else {
            if (i == (a - 1) * b)
                den = 0;
            if (den >= a) {
                countB++;
                den = 0;
            }
            if (graphe.at(i)) {
                line[2 * i].position = Vector2f(wid * (den + 1), len * (countB + 1));
                line[2 * i + 1].position = Vector2f((den + 1) * wid, len * (countB + 2));
            }
        }
        den++;
    }
    window.draw(line);
    window.display();

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }
    }
}
