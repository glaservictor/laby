#include "../include/mybitset.h"

#define un (uint64_t)1u 

/*	Affiche toutes les arêtes dans le flux sortant standard
 */
void dBit::display() {
	for(int i = 0; i < sz; i++)
		std::cout << (int)at(i);
	std::cout << '\n';
}

/* Renvoie un couple (n,m) sous forme de std::pair décrivant les dimensions du graph
 * @return pair<int, int>(n, m)
 */
std::pair<int, int> dBit::getDim(){
	return std::make_pair(a, b);
}

/* Renvoie le nombre d'arêtes du graph, pouvant servir à avoir une borne théorique (on peut parfos l'excéder un peu) pour une boucle for
 * @return le nombre d'arêtes
 * */
int dBit::size(){
	return sz;
};

/* Change l'état d'une arête
 * @param son indice dans notre système d'indexage
 * @param un 1 ou un 0 (ou un booléen) selon que l'arêt sera ouverteou non
 */
void dBit::setAt(int index, bool val) {
	if(val)
		buf[index/64] |= (un << index%64);
	else
		if(at(index)) 
			buf[index/64] ^= (un << index%64);
}

/* Récupère l'état d'une arête
 * @param l'indexe del'arête
 * @return son état sous forme d'un booléen
 */
bool dBit::at(int index) {
	return buf[index/64] & (un << (index%64));
}

/* Ca compare en temps linéaire en le nombre d'arêtes mais avec une constante cachée de 2e-6
 */
bool dBit::operator==(const dBit &cmp){
	if(a != cmp.a || b != cmp.b)
		return false;
	for(int i = 0; i < nbChnk-1; i++)
		if(buf[i]!=cmp.buf[i])
			return false;
	int offset = (nbChnk-1) << 6;
	for(int i = 0; i < sz%64; i++)
		if(at(offset+i) != ((dBit)cmp).at(offset+i))
			return false;
	return true;
}

/* Alloue un conteneur pour un graphe de n*m arêtes. 
 * @param n
 * @param m
 */
dBit::dBit(int n, int m) {
	a = n;
	b = m;
	sz = (2*m*n-m-n);
	nbChnk = sz/64+1;
	buf = (uint64_t*)malloc(nbChnk*sizeof(uint64_t));
}

/* Alloue et initialise un conteneur pour un graphe de n*m arêtes.
 * @param n
 * @param m
 * @param l'état intial des arêtes (0 ou 1) 
 */
dBit::dBit(int n, int m, bool st) {
		a = n;
		b = m;
		sz = (2*m*n-m-n);
		nbChnk = sz/64+1;
		buf = (uint64_t*)calloc(nbChnk, sizeof(uint64_t));
		if(st)
			for(int i = 0; i < nbChnk; i++)
				buf[i] = ~buf[i];
}

/* Destructeur qui se charge de désallouer la mémoire
 */
dBit::~dBit(){
	free(buf);
}
