#ifndef MYBISET_H_INCLUDED
#define MYBISET_H_INCLUDED

#include <stdint.h>
#include <utility>
#include <iostream>

class dBit{
	
	private :

	uint64_t *buf;
	int a, b, sz, nbChnk;

	public:

	void display();
	bool at(int index);
	void setAt(int index, bool val);
	std::pair<int, int> getDim();
	int size();
	bool operator==(const dBit &b);
	dBit(int n, int m);
	dBit(int n, int m, bool st);
	~dBit();
};

#endif
