#ifndef LABY_LABYGEN_H
#define LABY_LABYGEN_H

#include <bits/stdc++.h>
#include "mybitset.h"

void soluce(dBit &graphe, std::vector<int> &sel);

void addSommet(int p1);

#endif //LABY_LABYGEN_H
