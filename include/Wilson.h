
#ifndef LABY_WILSON_H
#define LABY_WILSON_H

#include <bits/stdc++.h>
#include "mybitset.h"



using namespace std;

vector<int> generation(dBit &graphe);

void soluceWilson(dBit &graphe, vector<int> res);

void RandWalk(vector<int> &sommet, const dBit &graphe, vector<int> &order, vector<int> &UST);

int getAccess(int cur, dBit graphe);


#endif //LABY_WILSON_H
