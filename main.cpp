#include <stdlib.h>
#include "include/SFML_interface.h"
#include "include/LabyGen.h"
#include  "include/mybitset.h"
#include <SFML/Window.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "include/Wilson.h"
#include "include/enum.h"

using namespace std;

int main() {
    srand(time(NULL));
    dBit graphe(30, 40 , false);
    vector<int> res = generation(graphe);
    soluceWilson(graphe, res);
    unionFind();
    return 0;
}
